from django.core.management.base import BaseCommand
from faker import Faker


class Command(BaseCommand):
    help = 'For example: students UK 5'

    def add_arguments(self, parser):
        parser.add_argument('country', type=str)
        parser.add_argument('amount', type=int)

    @staticmethod
    def student_generator(country, amount):
        fake = Faker(country)
        students = [{'first_name': fake.first_name(),
                     'last_name': fake.last_name(),
                     'email': fake.email(),
                     'password': fake.password(),
                     'birthday': fake.date_between(start_date="-40y", end_date="-18y")} for _ in range(amount)]
        return students

    @staticmethod
    def students_to_str(students_list):
        students_string = ''
        for index, student in enumerate(students_list, start=1):
            students_string += f'{index}. First name: {student["first_name"]:<15} Last name: ' \
                               f' {student["last_name"]:<15}  E-mail: {student["email"]:<25} \
            password: {student["password"]:<20} Birthday: {student["birthday"]} \n'
        return students_string

    def handle(self, *args, **options):
        students_list = self.student_generator(options['country'], options['amount'])
        students_str = self.students_to_str(students_list)
        return students_str

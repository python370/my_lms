from django.conf import settings
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, RedirectView, TemplateView, UpdateView

from core.form import RegistrationForm, ProfileForm, LoginForm
from core.services.emails import send_registration_email
from core.helpers.token_generator import TokenGenerator
from core.helpers.create_profile import create_profile

from core.models import Profile


def index(request: object) -> HttpResponse:
    """
    Entry point for "/" index url
    :param request: object
    :return: HttpResponse
    """
    return render(request,
                  template_name="index.html",
                  context={"meta": {"h1": "Best LMS in the World :)))))",
                                    "title": "Best LMS in the World | LMS",
                                    "description": "LMS system index page | LMS"}})


class Login(LoginView):
    form_class = LoginForm
    next_page = reverse_lazy("index")


class Logout(LogoutView):
    next_page = reverse_lazy("login")


class Registration(CreateView):
    template_name = "registration/registration.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Error validation. Incorrect data")
        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user, backend='django.contrib.auth.backends.ModelBackend')
            create_profile(current_user)  # temporary solution
            return super().get(request, *args, **kwargs)

        return HttpResponse("We can't activate you. Incorrect token")


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "profile.html"


class EditProfileView(LoginRequiredMixin, UpdateView):
    model = Profile
    pk_url_kwarg = "pk"
    form_class = ProfileForm
    template_name = "form_update.html"
    meta_page = {"title": "Update profile user | LMS", "description": "Update profile user | LMS",
                 "h1": "Update profile"}
    extra_context = {"page": "profile", "meta": meta_page}
    success_url = reverse_lazy("profile")

    def get(self, request, *args, **kwargs):
        if request.user.profile.id != kwargs["pk"]:
            return HttpResponse("Wrong! Incorrect profile id")
        return super().get(request, *args, **kwargs)


@login_required(login_url=reverse_lazy("login"))
def send_test_email(request):
    send_mail(
        subject="Baston Sergey  Hello from LMS ",
        message=f"Baston Sergey. Now - {timezone.now()}",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=settings.RECIEVERS_EMAIL,
    )
    return HttpResponse("Done")


# 404
def page_not_founds(request: object, exception: Exception):
    """Processing error 404. Custom page"""
    return render(request, "404.html", context={"h1": "Huston, we have a problem"})


# 500
def server_error(request: object):
    """Processing error 404. Custom page"""
    return render(request, "500.html")

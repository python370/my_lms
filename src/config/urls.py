"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import django
from django.contrib import admin
from django.template.defaulttags import url
from django.urls import path, include
from django.conf import settings
from django.views.generic import TemplateView
from core.views import (index,
                        page_not_founds,
                        server_error, Login,
                        Logout,
                        Registration,
                        send_test_email,
                        ActivateUser,
                        ProfileView,
                        EditProfileView)
from django.conf.urls.static import static


urlpatterns = [
    path("admin/", admin.site.urls),
    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("registration/", Registration.as_view(), name="registration"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    path("accounts/profile/", ProfileView.as_view(), name="profile"),
    path("edit-profile/<int:pk>/", EditProfileView.as_view(), name="edit_profile"),
    path("", index, name="index"),
    path("students/", include("students.urls")),
    path("teachers/", include("teachers.urls")),
    path("groups/", include("groups.urls")),
    path("send-email/", send_test_email, name="send_email"),
    path('oauth/', include('social_django.urls', namespace="social")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = page_not_founds
handler500 = server_error

from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from teachers.models import Teachers


@admin.register(Teachers)
class TeachersAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "email", "birth_date", "groups_count", "link_to_group"]
    list_filter = ["email", "birth_date", "last_name", "first_name"]
    search_fields = ["email", "first_name", "last_name"]
    search_help_text = "Search by email / First name / Last name"

    fieldsets = (
        (
            "Main info", {
                "fields": ("first_name", "last_name", "email", "groups")
            },
        ),
        (
            "Additional info", {
                "fields": ("avatar",)
            }
        )
    )

    def groups_count(self, obj):
        if obj.groups:
            return obj.groups.count()
        return format_html("<strong>0</strong>")

    def link_to_group(self, obj):
        if obj.groups:
            groups = obj.groups.all()
            link_list = []
            for group in groups:
                link_list.append(f"<a class='button' href='{reverse('admin:groups_groups_change', args=[group.pk])}'> \
                {group.programm_lang}</a>")
            return format_html("</br>".join(link_list))
        return format_html("<strong>Without groups </strong>")

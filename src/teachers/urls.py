from django.urls import path

from teachers.views import CreateTeachersView, UpdateTeacherView, AllTeachersView, DeleteTeacherView

app_name = "teachers"
urlpatterns = [
    path("", AllTeachersView.as_view(), name="all_teachers"),
    path("create/", CreateTeachersView.as_view(), name="create_teacher"),
    path("update/<int:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
]

from django.forms import ModelForm
from .models import Teachers
from django.core.validators import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class TeacherForm(ModelForm):
    class Meta:
        model = Teachers
        fields = "__all__"

    def clean(self):
        clean_data = super().clean()
        clean_data["first_name"] = str(clean_data["first_name"]).capitalize()
        clean_data["last_name"] = str(clean_data["last_name"]).capitalize()
        if ".ru" in str(clean_data["email"]).lower():
            raise ValidationError("You can't use russian service of email")

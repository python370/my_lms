from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView

from .models import Teachers
from django.urls import reverse_lazy
from .forms import TeacherForm


class CreateTeachersView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("login")
    model = Teachers
    form_class = TeacherForm
    template_name = "form_create.html"
    meta_page = {"title": "Create Teacher | LMS", "description": "Create teacher form | LMS", "h1": "Create teacher"}
    extra_context = {"page": "student", "meta": meta_page}
    success_url = reverse_lazy("teachers:all_teachers")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("login")
    model = Teachers
    form_class = TeacherForm
    pk_url_kwarg = "pk"
    template_name = "form_update.html"
    meta_page = {"title": "Update teacher | LMS", "description": "Update teacher form | LMS", "h1": "Update student"}
    extra_context = {"page": "teachers", "meta": meta_page}
    success_url = reverse_lazy("teachers:all_teachers")


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("login")
    model = Teachers
    pk_url_kwarg = "pk"
    success_url = reverse_lazy("teachers:all_teachers")


class AllTeachersView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login")

    def get(self, request, *args, **kwargs):
        context = super().get_context_data()
        if request.GET.get("search"):
            teacher = Teachers.objects.filter(first_name=str(request.GET["search"]).strip().capitalize())
            context["meta"] = {
                "title": f"Search teacher {str(request.GET['search'])} | LMS",
                "description": "Search teacher | LMS",
                "h1": "Search teachers",
            }
            self.template_name = "teachers/teacher_search.html"
        else:

            teacher = Teachers.objects.all()
            context["meta"] = {"title": "All teacher | LMS", "description": "All teacher | LMS", "h1": "Teachers"}
            self.template_name = "teachers/output_teachers.html"
        context["obj_from_db"] = teacher
        return self.render_to_response(context)

import random
from uuid import uuid4

from django.db import models
from faker import Faker
from core.models import Person


class Teachers(Person):
    groups = models.ManyToManyField("groups.Groups")
    avatar = models.ImageField(upload_to="image/teachers/", null=True)

    @classmethod
    def generate_teachers(cls, amount):
        """
           Generate teachers from Faker
           :param count: int - amount teachers generated
           :return:
        """
        faker = Faker()
        for _ in range(amount):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                password=faker.password(),
                birth_date=faker.date_time_between(start_date="-50y", end_date="-25y"),
                phone=faker.phone_number(),
                programm_lang=random.choice(["Python", "PHP", "Perl", "Go", "C++"]),
                amount_group=1,
                practice_year=7,
            )

    def __str__(self):
        return f"id {self.id} name: {self.first_name} last name: {self.last_name}"

    class Meta:
        verbose_name = "Teacher"
        verbose_name_plural = "All Teachers"

from django.contrib import admin

from students.models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "grade", "email", "birth_date", "group"]
    ordering = ("-grade",)
    list_filter = ["group", "grade", "email", "birth_date", "last_name", "first_name"]
    search_fields = ["email", "first_name", "last_name"]
    search_help_text = "Search by email / First name / Last name"
    fieldsets = (
        (None, {"fields": ("first_name", "last_name", "email", "grade")}),
        ("Additional info", {"fields": ("birth_date", "group")})
    )

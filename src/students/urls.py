from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from students.views import AllStudentsView, CreateStudentView, UpdateStudentView, DeleteStudentView, get_resume

app_name = "students"
urlpatterns = [
    path("", AllStudentsView.as_view(), name="all_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:pk>/", DeleteStudentView.as_view(), name="delete_student"),
    path("resume/<uuid:pk>", get_resume, name="get_resume"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

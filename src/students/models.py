from datetime import datetime
from uuid import uuid4

from django.core import validators
from django.utils import timezone
from django.db import models
from faker import Faker
from django.conf import settings
from core.models import Person


class Student(Person):
    ACTIVE = "Active"
    GRADUATE = "Graduate"
    EXPELLED = "Expelled"
    STATUS_STUDENT = [
        (ACTIVE, "Active student"),
        (GRADUATE, "Graduated successfully"),
        (EXPELLED, "Expelled from study"),
    ]

    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
        unique=True,
        db_index=True,
    )

    status = models.CharField(max_length=10, choices=STATUS_STUDENT, default=ACTIVE, blank=False)
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    group = models.ForeignKey(to="groups.Groups", on_delete=models.CASCADE, null=True)
    avatar = models.ImageField(upload_to="image/students/", null=True)
    resume = models.FileField(
        upload_to="resume/students/",
        validators=[validators.FileExtensionValidator(["pdf", "doc"], message="Only PDF or DOC files")],
        null=True,
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "All students"

    @classmethod
    def generate_students(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
                grade=faker.random.randint(0, 100),
            )

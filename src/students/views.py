import uuid

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView

from .models import Student
from django.urls import reverse_lazy
from .forms import StudentForm


class CreateStudentView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("login")
    model = Student
    form_class = StudentForm
    template_name = "form_create.html"
    meta_page = {"title": "Create student | LMS", "description": "Create student form | LMS", "h1": "Create student"}
    extra_context = {"page": "student", "meta": meta_page}
    success_url = reverse_lazy("students:all_students")


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("login")
    model = Student
    form_class = StudentForm
    pk_url_kwarg = "pk"
    template_name = "form_update.html"
    meta_page = {"title": "Update student | LMS", "description": "Update student form | LMS", "h1": "Update student"}
    extra_context = {"page": "student", "meta": meta_page}
    success_url = reverse_lazy("students:all_students")


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("login")
    model = Student
    pk_url_kwarg = "pk"
    success_url = reverse_lazy("students:all_students")


class AllStudentsView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login")

    def get(self, request, *args, **kwargs):
        context = super().get_context_data()
        if request.GET.get("search"):
            student = Student.objects.filter(first_name=str(request.GET["search"]).strip().capitalize())
            context["meta"] = {
                "title": f"Search student {str(request.GET['search'])} | LMS",
                "description": "Search student | LMS",
                "h1": "Search students",
            }
            self.template_name = "students/student_search.html"
        else:
            student = Student.objects.all()
            context["meta"] = {"title": "All Student | LMS", "description": "All student | LMS", "h1": "Students"}
            self.template_name = "students/student_output.html"
        context["obj_from_db"] = student

        return self.render_to_response(context)


@login_required(login_url=reverse_lazy("login"))
def get_resume(request: object, pk: uuid.UUID):
    student = get_object_or_404(Student.objects.filter(pk=pk))
    return FileResponse(student.resume)

from django.forms import ModelForm
from .models import Student
from django.core.validators import ValidationError


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = "__all__"

    def clean_email(self):
        email = self.cleaned_data["email"]
        if ".ru" in str(self.cleaned_data["email"]).lower():
            raise ValidationError("You can't use russian service of email")
        return email

    def clean_first_name(self):
        return self.clean_names(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.clean_names((self.cleaned_data["last_name"]))

    def clean(self):
        cleaned_data = super().clean()
        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]
        if first_name == last_name:
            raise ValidationError("Bro, you can't use equal First Name and Last Name. OK? Try Again! ")

    @staticmethod
    def clean_names(string_data: str) -> str:
        return string_data.strip().capitalize()

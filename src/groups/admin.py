from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from groups.models import Groups

from students.models import Student
from teachers.models import Teachers
import core.helpers.const as const


class StudentAdminInLine(admin.TabularInline):
    model = Student
    extra = 0


class TeacherAdminInLines(admin.TabularInline):
    model = Teachers.groups.through
    extra = 0


@admin.register(Groups)
class GroupsAdmin(admin.ModelAdmin):
    list_display = ["programm_lang", "start_date", "end_date", "group_num", "students_count", "all_student"]
    list_filter = ["programm_lang", "start_date", "end_date"]
    search_fields = ["programm_lang"]
    search_help_text = "Search language"
    fieldsets = (
        (
            None, {
                "fields": ("programm_lang", "start_date", "end_date")
            },
        ),

    )
    inlines = [StudentAdminInLine, TeacherAdminInLines]

    def students_count(self, obj):
        students = Student.objects.filter(group=obj.id)
        return format_html(f"{len(students)}")

    def all_student(self, obj):
        students = Student.objects.filter(group=obj.id)
        student_links = []
        for student in students:
            student_links.append(f"<li><span  class='dropdown-item'><a \
            href={reverse('admin:students_student_change', args=[student.pk])}> {student.last_name} </a> |"
                                 f"<a href='{reverse('admin:students_student_delete', args=[student.pk])}'> \
                                  >Del< </a></span></li>")
        return format_html(const.HTML_DROPDOWN_OPEN + ''.join(student_links) + const.HTML_DROPDOWN_CLOSE)

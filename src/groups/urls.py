from django.urls import path

from groups.views import CreateGroupView, UpdateGroupView, AllGroupsView, DeleteGroupView, ShowGroupView

app_name = "groups"
urlpatterns = [
    path("", AllGroupsView.as_view(), name="all_groups"),
    path("create/", CreateGroupView.as_view(), name="create_group"),
    path("update/<int:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<int:pk>/", DeleteGroupView.as_view(), name="delete_group"),
    path("show/<int:pk>/", ShowGroupView.as_view(), name="show_group"),
]

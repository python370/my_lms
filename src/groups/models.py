import random

from django.db import models

from faker import Faker
from django.conf import settings


class Groups(models.Model):
    PROGRAMM_LANG = settings.PROGRAMMING_LANG_TUPLE
    programm_lang = models.CharField(choices=PROGRAMM_LANG, default="Python", max_length=20, null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    amount_student = models.PositiveSmallIntegerField(default=5, null=True)
    teacher_id = models.IntegerField(null=True)
    group_num = models.IntegerField(null=True)

    @classmethod
    def generate_groups(cls, amount):
        faker = Faker()
        for _ in range(amount):
            cls.objects.create(
                programm_lang="Python",
                start_date=faker.date_time_between(start_date="-10d"),
                end_date=faker.date_time_between(start_date="-10d"),
                amount_student=random.randint(5, 15),
                teacher_id=1,
                group_num=1,
            )

    def __str__(self):
        return f"{self.programm_lang}. Amount students: {self.amount_student}"

    class Meta:
        verbose_name = "Group"
        verbose_name_plural = "All groups"

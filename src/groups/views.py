from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView

from .models import Groups
from .forms import GroupForm


class CreateGroupView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("login")
    model = Groups
    form_class = GroupForm
    template_name = "form_create.html"
    meta_page = {"title": "Create group | LMS", "description": "Create group form | LMS", "h1": "Create student"}
    extra_context = {"page": "group", "meta": meta_page}
    success_url = reverse_lazy("groups:all_groups")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("login")
    model = Groups
    form_class = GroupForm
    pk_url_kwarg = "pk"
    template_name = "form_update.html"
    meta_page = {"title": "Update group | LMS", "description": "Update group form | LMS", "h1": "Update student"}
    extra_context = {"page": "group", "meta": meta_page}
    success_url = reverse_lazy("groups:all_groups")


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("login")
    model = Groups
    pk_url_kwarg = "pk"
    success_url = reverse_lazy("groups:all_groups")


class AllGroupsView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login")
    template_name = "groups/output_groups.html"

    def get(self, request, *args, **kwargs):
        context = super().get_context_data()
        groups = Groups.objects.all()
        context["meta"] = {"title": "All Student | LMS", "description": "All student | LMS", "h1": "Students"}
        context["obj_from_db"] = groups
        return self.render_to_response(context)


class ShowGroupView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login")
    template_name = "groups/show_group.html"

    def get(self, request, *args, **kwargs):
        context = super().get_context_data()
        group = get_object_or_404(Groups.objects.all(), pk=kwargs["pk"])
        context["meta"] = {"title": "Group info | LMS", "description": "Group info | LMS", "h1": "Group"}
        context["group"] = group
        return self.render_to_response(context)

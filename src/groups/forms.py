from django.forms import ModelForm
from .models import Groups


class GroupForm(ModelForm):
    class Meta:
        model = Groups
        fields = (
            "programm_lang",
            "amount_student",
            "group_num",
            "teacher_id",
            "start_date",
            "end_date",
        )
